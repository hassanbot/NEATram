import random
import time
import matplotlib.pyplot as plt

from evolution import Individual
from netgraph import NetGraph


# Parameters
n_inputs = 2
n_outputs = 4
n_genes = (n_inputs + 1) * n_outputs

# Create individual
ind = Individual(n_inputs, n_outputs)
ind.enable_all_connections()

# Setup graph
graph = NetGraph()
plt.ion()
fig, _ = plt.subplots()

# Add two neurons and a connection
new_neuron = ind.add_neuron(ind.genome[0])
new_connection = ind.add_connection(new_neuron, ind.neurons['outputs'][2], weight=2, enabled=True)
new_neuron = ind.add_neuron(new_connection)

# Perform feed-forward and draw the graph
for i in xrange(0, 100):
    input_values = [random.random() for _ in xrange(n_inputs)]
    ind.feed_forward(input_values)

    # Create and draw the graph

    fig.clear()
    graph.draw_graph(ind)
    fig.canvas.draw()
    fig.canvas.flush_events()

    time.sleep(1)
