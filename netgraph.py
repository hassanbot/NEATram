import networkx as nx


class NetGraph(nx.Graph):
    def __init__(self):
        super(NetGraph, self).__init__()
        self.center_y = 0.0  # Y coordinate center position
        self.distance_x = 5.0  # Distance in x-direction of each layer
        self.distance_y = 1.0  # Distance in y-direction of each node

    def draw_graph(self, network):
        neuron_names = []
        neuron_labels = {}
        neuron_positions = {}

        all_neurons = network.get_all_neurons()

        for i, neuron in enumerate(all_neurons):
            neuron_name = str(neuron.id)
            neuron_names.append(neuron_name)
            neuron_labels[neuron_name] = "{}\n{:.2f}".format(neuron.id, neuron.value)
            neuron_positions[neuron_name] = neuron.position
            self.add_node(str(neuron.id))

        # Add connections
        c_labels = {}
        for connection in network.genome:
            if connection.enabled:
                c_tuple = (str(connection.from_neuron.id),
                           str(connection.to_neuron.id))
                c_labels[c_tuple] = str(connection.weight)
                self.add_edge(*c_tuple)

        # Draw network
        nx.draw_networkx_nodes(self, pos=neuron_positions, node_size=600)
        nx.draw_networkx_labels(self, pos=neuron_positions, labels=neuron_labels, font_size=8)
        nx.draw_networkx_edges(self, pos=neuron_positions)
        nx.draw_networkx_edge_labels(self, pos=neuron_positions,
                                     edge_labels=c_labels,
                                     label_pos=0.15,
                                     font_size=8)
