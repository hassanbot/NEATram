import unittest
from evolution import Individual, Population


class IndividualTest(unittest.TestCase):
    def setUp(self):
        self.n_inputs = 2
        self.n_outputs = 4
        self.n_genes = (self.n_inputs + 1) * self.n_outputs
        self.ind = Individual(self.n_inputs, self.n_outputs)

    def test_setup(self):
        self.assertEqual(self.n_genes, 12)
        self.assertEqual(len(self.ind.genome), self.n_genes)
        self.assertEqual(len(self.ind.neurons['inputs']), 3)
        self.assertEqual(len(self.ind.neurons['hidden']), 0)
        self.assertEqual(len(self.ind.neurons['outputs']), 4)

    def test_add_node_and_connection(self):
        # Add a node
        connection = self.ind.genome[0]
        self.ind.add_neuron(connection)
        self.assertEqual(len(self.ind.neurons['inputs']), 3)
        self.assertEqual(len(self.ind.neurons['hidden']), 1)
        self.assertEqual(len(self.ind.neurons['outputs']), 4)
        self.assertEqual(len(self.ind.genome), self.n_genes+2)
        self.assertEqual(self.ind.genome[-1].enabled, True)
        self.assertEqual(self.ind.genome[-2].enabled, True)
        self.assertEqual(connection.enabled, False)

        # Add a connection
        self.ind.add_connection(self.ind.neurons['inputs'][1], self.ind.neurons['hidden'][0], weight=3.5)
        self.ind.add_connection(self.ind.neurons['hidden'][0], self.ind.neurons['outputs'][2], enabled=True)
        self.assertEqual(len(self.ind.genome), self.n_genes+4)
        self.assertEqual(self.ind.genome[-1].enabled, True)
        self.assertEqual(self.ind.genome[-1].weight, 1)
        self.assertEqual(self.ind.genome[-2].enabled, False)
        self.assertEqual(self.ind.genome[-2].weight, 3.5)

        # Try adding a connection that already exists
        self.ind.add_connection(self.ind.neurons['inputs'][1], self.ind.neurons['hidden'][0])
        self.assertEqual(len(self.ind.genome), self.n_genes+4)
        self.assertEqual(self.ind.genome[-2].enabled, False)
        self.assertEqual(self.ind.genome[-2].weight, 3.5)

    def test_set_input_values(self):
        self.assertEqual(self.ind.neurons['inputs'][0].value, 1)
        self.assertEqual(self.ind.neurons['inputs'][1].value, None)
        self.assertEqual(self.ind.neurons['inputs'][2].value, None)

        # Set inputs
        input_values = [2, 3]
        self.ind.set_input_values(input_values)

        self.assertEqual(self.ind.neurons['inputs'][0].value, 1)
        self.assertEqual(self.ind.neurons['inputs'][1].value, input_values[0])
        self.assertEqual(self.ind.neurons['inputs'][2].value, input_values[1])

        # Try setting too many inputs
        input_values = [1, 2, 3]
        self.assertRaises(ValueError, self.ind.set_input_values, input_values)

        # Reset neurons
        self.ind.reset_neurons()

        self.assertEqual(self.ind.neurons['inputs'][0].value, 1)
        self.assertEqual(self.ind.neurons['inputs'][1].value, None)
        self.assertEqual(self.ind.neurons['inputs'][2].value, None)


class PopulationTest(unittest.TestCase):
    def setUp(self):
        self.pop_size = 5
        self.n_inputs = 2
        self.n_outputs = 2
        self.population = Population(self.pop_size,
                                     self.n_inputs,
                                     self.n_outputs)

    def test_overloading(self):
        self.assertEqual(len(self.population), 5)
        del self.population[3]
        self.assertEqual(len(self.population), 4)


if __name__ == '__main__':
    unittest.main()
